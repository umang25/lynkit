import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./features/pages/login/login.module').then(mod => mod.LoginModule),
    canActivate: [],
  },
  {
    path: 'login',
    loadChildren: () => import('./features/pages/login/login.module').then(mod => mod.LoginModule),
    canActivate: [],
  },
  {
    path: 'pages',
    loadChildren: () => import('./features/pages/pages.module').then(mod => mod.PagesModule),
    canActivate: [],
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes,{
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
