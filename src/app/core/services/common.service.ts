import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  saveSessionInLocal(payload) {
    console.log(payload);
    this.saveInLocal('email', payload.email);
  }

  removeSessionFromLocal() {
    this.removeFromLocal('email');
  }

  getFromLocal(key: string): string {
    return localStorage.getItem(key);
  }

  saveInLocal(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  removeFromLocal(key: string) {
    localStorage.removeItem(key);
  }
}
