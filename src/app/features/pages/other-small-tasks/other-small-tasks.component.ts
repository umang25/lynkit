import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-other-small-tasks',
  templateUrl: './other-small-tasks.component.html',
  styleUrls: ['./other-small-tasks.component.scss']
})
export class OtherSmallTasksComponent implements OnInit {
  todayNumber: number = Date.now();
  todayDate : Date = new Date();
  todayString : string = new Date().toDateString();
  todayISOString : string = new Date().toISOString();
  propertyBinding = true;

  constructor(private router: Router) { }

  ngOnInit(): void {
    //PropertyBinding Example
    setTimeout(() =>{  
      this.propertyBinding = false;  
    }, 4000);  
  }

  routeToRegistrationForm() { 
    this.router.navigate(['../pages/registration-form']);
  }
}
