import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { OtherSmallTasksComponent } from './other-small-tasks.component';

const routes: Routes = [
  {
      path: '',
      component: OtherSmallTasksComponent,
  }
];

@NgModule({
  declarations: [OtherSmallTasksComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class OtherSmallTasksModule { }
