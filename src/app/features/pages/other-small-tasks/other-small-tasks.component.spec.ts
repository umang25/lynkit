import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherSmallTasksComponent } from './other-small-tasks.component';

describe('OtherSmallTasksComponent', () => {
  let component: OtherSmallTasksComponent;
  let fixture: ComponentFixture<OtherSmallTasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherSmallTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherSmallTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
