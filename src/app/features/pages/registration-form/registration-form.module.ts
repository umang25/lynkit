import { RegistrationFormComponent } from './registration-form.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'registration-form',
    pathMatch: 'full'
  },
  {
    path: 'registration-form',
    component: RegistrationFormComponent,
  }

];
@NgModule({
    declarations: [RegistrationFormComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        FormsModule,
    ],
    providers: []
})
export class RegistrationFormModule { }