import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './../../pages.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./../../home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'home',
        loadChildren: () => import('./../../home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'registration-form',
        loadChildren: () => import('./../../registration-form/registration-form.module').then(m => m.RegistrationFormModule),
      },
      {
        path: 'other-tasks',
        loadChildren: () => import('./../../other-small-tasks/other-small-tasks.module').then(m => m.OtherSmallTasksModule),
      },
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes), CommonModule
  ],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
